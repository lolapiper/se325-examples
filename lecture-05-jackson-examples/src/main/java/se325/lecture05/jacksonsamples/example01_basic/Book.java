package se325.lecture05.jacksonsamples.example01_basic;

import java.util.Objects;

public class Book {

    private String title;

    private Genre genre;

    public Book() {}

    public Book(String title, Genre genre) {
        this.title = title;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}
