package se325.lecture05.jacksonsamples;

import com.fasterxml.jackson.databind.ObjectMapper;
import se325.lecture05.jacksonsamples.example07_polymorphism.Cat;
import se325.lecture05.jacksonsamples.example07_polymorphism.Dog;
import se325.lecture05.jacksonsamples.example07_polymorphism.Zoo;

import java.io.IOException;

public class JacksonSamplesMain {

    public static void main(String[] args) throws IOException {

//        Movie book = new Movie("The Neverending Story", LocalDate.of(2000, 11, 21));

        Zoo theZoo = new Zoo();
        theZoo.add(new Cat("Fluffy"));
        theZoo.add(new Dog("Lassie"));
        System.out.println(theZoo);

        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(theZoo);

        System.out.println(json);

        Zoo deserializedZoo = mapper.readValue(json, Zoo.class);
        System.out.println(deserializedZoo);

//        Movie deserializedBook = mapper.readValue(json, Movie.class);
//
//        System.out.println(book.equals(deserializedBook));

    }
}
