package se325.examples.auction.onetoone;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Item_onetoone {

    @Id
    @GeneratedValue
    private Long _id;

    private String _name;


    protected Item_onetoone() {
    }

    public Item_onetoone(String name) {
        _name = name;
    }

    public Long getId() {
        return _id;
    }

    public String getName() {
        return _name;
    }
}
